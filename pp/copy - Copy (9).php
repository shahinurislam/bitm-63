  <html>

<body>

<!--// html form create-->

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

    First Name: <input type="text" name="first_name">

    Second Name: <input type="text" name="second_name">

    <input type="submit">

</form>

<!--// html form end-->

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST")                             //REQUEST is used to collect data after submitting an HTML form.

{

    $first_name = $_REQUEST['first_name'];                            // first input REQUEST

    $second_name = $_REQUEST['second_name'];                          // second input REQUEST

 

    if (empty($first_name))                                           // condition

    {

        echo "First Name is empty";                                   // output

    }

    elseif (empty($second_name))                                      // condition

    {

        echo "Second Name is empty";                                  // output

    }

    else

    {

        echo "First Name: " . $first_name;                            // output

        echo "<br>";                                                  // single line break;

        echo "Second Name: " . $second_name;                           // output

    }

}

?>

 

</body>

</html>