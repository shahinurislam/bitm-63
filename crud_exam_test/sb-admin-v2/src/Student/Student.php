<?php
namespace App\Student;
use App\Connection;
use PDO;
use PDOException;
class Student extends Connection{
	private $name;
	private $department;
	private $address;
	private $id;
	public function set($data = array())
    {
       if(array_key_exists('name', $data))
       {
           $this->name = $data['name'];
       }
        if(array_key_exists('department', $data))
        {
            $this->department = $data['department'];
        }
        if(array_key_exists('address', $data))
        {
            $this->address = $data['address'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
       return $this;
    }

    public function store(){
        try {
           $stmt = $this->con->prepare("INSERT INTO `oop_students` (`name`,`department`,`address`) VALUES(:n,:department,:a)");
           $result = $stmt->execute(array(
                ':n'=>$this->name,
                ':department'=>$this->department,
                ':a'=>$this->address
            ));
           if($result){
               header('location: index.php');
           }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}