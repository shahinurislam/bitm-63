<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
	
	
	<div class="container">
		<div class="row">
			
			<div class="col-md-3"></div>
			<div class="col-md-6">
			
					<form action="store.php" method="post" enctype="multipart/form-data">
					
						  <div class="form-group">
							<label >Name :</label>
							<input type="text" class="form-control" name="username">
							<small id="emailHelp" class="form-text text-muted"></small>
						  </div>
						  
						  <div class="form-group">
							<label >Email address :</label>
							<input type="text" class="form-control" name="email">
							<small id="emailHelp" class="form-text text-muted"></small>
						  </div>
						  
						  <div class="form-group">
							<label >Password :</label>
							<input type="password" class="form-control" name="pass">
						  </div>	 

						  <div class="form-group">
							<label >Enter Your Address :</label>
							<input type="textarea" class="form-control" name="address">
						  </div>
						  
						  <div class="form-group">
							<label >Enter Your Phone Number :</label>
							<input type="text" class="form-control" name="number">
						  </div>
						  
						  	  
						  <div class="form-group">
						  <label >Select Your Gender :</label>
						  </div>
								<div class="form-group">

								<label class="custom-control custom-radio">
								  <input id="radio1" name="radio" type="radio" class="custom-control-input"  value="Male">
								  <span class="custom-control-indicator"></span>
								  <span class="custom-control-description">Male</span>
								</label>
								<label class="custom-control custom-radio">
								  <input id="radio2" name="radio" type="radio" class="custom-control-input"  value="Female">
								  <span class="custom-control-indicator"></span>
								  <span class="custom-control-description">Female</span>
								</label>
								
						  </div> 
						  
						  
						  <div class="form-group">
							<label >Skill :</label>
						  </div>
						  
						  
						  <div class="form-group">
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="skill[]" value="php">
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">php</span>
								</label>
							
					
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="skill[]" value="css">
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">css</span>
								</label>
							
						
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="skill[]" value="html">
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">html</span>
								</label>
							
						  </div>
						
						  

						  <div class="form-group">
							<label >Choose your hobbies :</label>
						  </div>
						  
						  
						  <div class="form-group">
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="hobbie[]" value="cricket">
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">cricket</span>
								</label>
						
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="hobbie[]" value="football">
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">football</span>
								</label>
							
					
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="hobbie[]" value="kabadi">
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">kabadi</span>
								</label>
							
						  </div>
						  
						    <div class="form-group">
							<label >Date of Birth :</label>
							<div class="row">
								<div class="col-md-4">
									<select class="form-control" name="birth[]" value="day">
							  <option>1</option>
							  <option>2</option>
							
							</select>	
								</div>
								
							
								
								<div class="col-md-4">
									<select class="form-control" name="birth[]" value="month">
							  <option>January</option>
							  <option>Feb</option>
							
							</select>	
								</div>
								
								
							
								<div class="col-md-4">
								<select class="form-control" name="birth[]" value="years">
							  <option>2017</option>
							  <option>2018</option>
							
							</select>	
								</div>
								
						
							
							
							
						  </div>
						  </div>
						  
						  
						  <div class="form-group">
								<label class="custom-file">
								  <input type="file" id="file2" class="custom-file-input" name="image">
								  <span class="custom-file-control"></span>
								</label>
							
						  </div>
				
						  
						  <button type="submit" class="btn btn-primary">Submit</button>
						</form>
			
		
			
			</div>
		</div>
	</div>
	
	

	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>