<?php

include_once '../includes/header.php';
include_once '../../vendor/autoload.php';
$students = new App\Student\Student();
$student = $students->index();

?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">All Students</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Form Elements
                        </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $i=0;
                                    foreach ($student as $astudent){
                                $i++;
                                ?>
                               
                                    <tr>
                                        <td><?= $i;?></td>
                                        <td><?php echo $astudent['name']?></td>
                                        <td><?php echo $astudent['department']?></td>
                                        <td><?php echo $astudent['address']?></td>


                                        <td class="center">
                                            <a href="view/student/view.php?id=<?php echo $astudent['id']?>">View</a> |
                                            <a href="view/student/edit.php?id=<?php echo $astudent['id']?>">Edit</a> |
                                            <a href="view/student/delete.php?id=<?php echo $astudent['id']?>">Delete</a>
                                        </td>
                                    </tr>
                           <?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
<?php
include_once '../includes/footer.php';
?>