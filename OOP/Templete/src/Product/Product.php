<?php
namespace App\Product;
use App\Connection;
use PDO;
use PDOException;
class Product extends Connection{
    private $name;
    private $category;
    private $description;
    private $images;
    private $id;
    public function set($data = array())
    {
        if(array_key_exists('name', $data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('category', $data))
        {
            $this->category = $data['category'];
        }
        if(array_key_exists('description', $data))
        {
            $this->description = $data['description'];
        }
        if(array_key_exists('images',$data)){
            $this->images = $data['images'];
        }

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {
            $stmt = $this->con->prepare("INSERT INTO `oop_students` (`name`,`department`,`address`) VALUES(:n,:department,:a)");
            $result = $stmt->execute(array(
                ':n'=>$this->name,
                ':department'=>$this->department,
                ':a'=>$this->address
            ));
            if($result){
                header('location: index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function index(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `oop_students`"); //update table name
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function view($id){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `oop_students` WHERE id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {
            $stmt = $this->con->prepare("DELETE FROM `oop_students` WHERE id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: index.php');
            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function update(){
        try {
            $stmt = $this->con->prepare("UPDATE `test`.`oop_students` SET `name` = :name, `department` = :department, `address` = :address WHERE `oop_students`.`id` = :id;"); //update table name
            $stmt->bindValue(':name', $this->name, PDO::PARAM_INT);
            $stmt->bindValue(':department', $this->department, PDO::PARAM_INT);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_INT);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }






}