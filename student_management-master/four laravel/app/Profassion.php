<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profassion extends Model
{
    protected  $table = 'professionals';
    protected $fillable = ['name', 'email'];
}
