<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
   public function index() {
       return view('admin.index');
   }
   public function std_add() {
       return view('admin.student.std_add');
   }
   public function view(){
       return view('admin.student.view');
   }
}
