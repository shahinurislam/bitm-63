<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('front.index');
    }
    public function admin(){
        return view('admin.index');
    }
    public function welcome(){
            return view('welcome');
    }

    public function about () {
        //echo 'This is about';
        return view('about');
    }
    public  function user($id,$name) {
        //echo 'This is user '.$id;
        return view('user.profile',compact('id','name'));
    }
}
