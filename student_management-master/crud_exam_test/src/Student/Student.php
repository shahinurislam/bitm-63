<?php
namespace App\Student;
use App\Connection;
use PDO;
use PDOException;
class Student extends Connection{
	private $name;
	private $department;
	private $address;
	private $image;
	private $uniqe_id;
	private $id;
	public function set($data = array())
    {
       if(array_key_exists('name', $data))
       {
           $this->name = $data['name'];
       }
        if(array_key_exists('department', $data))
        {
            $this->department = $data['department'];
        }
        if(array_key_exists('address', $data))
        {
            $this->address = $data['address'];
        }
        if(array_key_exists('image', $data))
        {
            $this->image = $data['image'];
        }
        if(array_key_exists('uniqe_id', $data))
        {
            $this->uniqe_id = $data['uniqe_id'];
        }
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
       return $this;
    }

    public function store(){
        try {
           $stmt = $this->con->prepare("INSERT INTO `oop_students` (`name`,`department`,`address`,`image`, `uniqe_id`) VALUES(:name,:department,:address,:image,:uniqe_id)");
           $result = $stmt->execute(array(
                ':name'=>$this->name,
                ':department'=>$this->department,
                ':address'=>$this->address,
                ':image'=>$this->image,
                ':uniqe_id'=> md5(time())
            ));
           if($result){
               header('location: index.php');
           }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function index(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `oop_students` WHERE `delete_at` = '0000-00-00 00:00:00'"); //update table name
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
 public function trash(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `oop_students` WHERE `delete_at` != '0000-00-00 00:00:00'"); //update table name
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function view($id){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `oop_students` WHERE uniqe_id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

//temporary delete-------------
    public function tmp_delete($id){
        try {
            $stmt = $this->con->prepare("UPDATE `test`.`oop_students` SET `delete_at` = NOW() WHERE uniqe_id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: index.php');
            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    //restore
    public function restore($id){
        try {
            $stmt = $this->con->prepare("UPDATE `test`.`oop_students` SET `delete_at` = '0000-00-00 00:00:00' WHERE uniqe_id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: trash.php');
            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }




    public function delete($id){
        try {
            $stmt = $this->con->prepare("DELETE FROM `oop_students` WHERE uniqe_id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: trash.php');
            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function update(){
        try {
            $stmt = $this->con->prepare("UPDATE `test`.`oop_students` SET `name` = :name, `department` = :department, `address` = :address, `image` = :image WHERE `oop_students`.`uniqe_id` = :id;"); //update table name
            $stmt->bindValue(':name', $this->name, PDO::PARAM_INT);
            $stmt->bindValue(':department', $this->department, PDO::PARAM_INT);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_INT);
            $stmt->bindValue(':image', $this->image, PDO::PARAM_INT);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }






}