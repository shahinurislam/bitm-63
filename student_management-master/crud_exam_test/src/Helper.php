<?php
/**
 * Created by PhpStorm.
 * User: Shahin
 * Date: 03/10/2017
 * Time: 11:53 AM
 */

namespace App;

use PDO;
use PDOException;
class Helper extends Connection
{

    public function image_delete($id){
        try {
            $stmt = $this->con->prepare("SELECT `image` FROM `oop_students` WHERE uniqe_id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            if(isset($data['image'])){
                unlink('../uploads/'.$data['image']);
            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function img_upload(){
        $temp_name = $_FILES['image']['tmp_name'];
        $img_name = $_FILES['image']['name'];
        $genName = substr(md5(time()),0,8);
        $extName = explode('.',$img_name);
        $_POST['image'] =  $genName.'.'.end($extName);;
        move_uploaded_file($temp_name,'../uploads/'.$_POST['image']);
        return $_POST['image'];
    }





}