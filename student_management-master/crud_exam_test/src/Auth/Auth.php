<?php

namespace App\Auth;
use App\Connection;
use PDO;
use PDOException;

class Auth extends Connection
{

    private $user;
    private $email;
    private $password;
    private $id;
    public function set($data = array())
    {
        if(array_key_exists('user', $data))
        {
            $this->user = $data['user'];
        }
        if(array_key_exists('email', $data))
        {
            $this->email = $data['email'];
        }
        if(array_key_exists('password', $data))
        {
            $this->password = $data['password'];
        }
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {
            $stmt = $this->con->prepare("INSERT INTO `users` (`user`,`email`,`password`) VALUES(:user,:email,:password )");
            $result = $stmt->execute(array(
                ':user'=>$this->user,
                ':email'=>$this->email,
                ':password'=>$this->password
            ));
            if($result){
                header('location: ../../index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function login(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE (user = :user || email = :email) && password = :password");
            $stmt->bindValue(':user', $this->user, PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':password', $this->password, PDO::PARAM_STR);
            $stmt->execute();
            $users = $stmt->fetch(PDO::FETCH_ASSOC);
           // var_dump($users);

            if(!empty($users['id'])){
                $_SESSION['user'] = $users['user'];
                header('location:../../index.php');

            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }








}