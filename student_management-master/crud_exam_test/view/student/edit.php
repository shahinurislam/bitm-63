<?php
include_once '../includes/header.php';
include_once '../../vendor/autoload.php';
$student = new App\Student\Student();
$students = $student->view($_GET['id']);



?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Update</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Basic Form Elements
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/student/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Student Name</label>
                                        <input name="name" value="<?php echo $students['name']?>" class="form-control">
                                        <input type="hidden" name="id" value="<?php echo $students['uniqe_id']?>" class="form-control">
                                        <input type="hidden" name="image" value="<?php echo $students['image']?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select name="department" id="" class="form-control">
                                            <option>Select One</option>
                                            <option <?php echo ($students['department']=='CSE')?'selected':'' ?> value="CSE">CSE</option>
                                            <option <?php echo ($students['department']=='BBA')?'selected':'' ?> value="BBA">BBA</option>
                                            <option <?php echo ($students['department']=='EEE')?'selected':'' ?> value="EEE">EEE</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea name="address" class="form-control" rows="3"><?php echo $students['address']?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Image</label>
                                        <input name="image" type="file" class="form-control" rows="3" />
                                    </div>
                                    <div  style="padding-bottom: 25px">
                                        <img width="60px" src="view/uploads/<?php echo $students['image']?>" />
                                    </div>
                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                    <button type="submit" class="btn btn-warning">Update</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../includes/footer.php';
?>