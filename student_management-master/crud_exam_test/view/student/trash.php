<?php

include_once '../includes/header.php';
include_once '../../vendor/autoload.php';
$students = new App\Student\Student();
$student = $students->trash();

?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">All Students</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Form Elements
                        </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Address</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $i=0;
                                    foreach ($student as $astudent){
                                $i++;
                                ?>
                               
                                    <tr>
                                        <td><?= $i;?></td>
                                        <td><?php echo $astudent['name']?></td>
                                        <td><?php echo $astudent['department']?></td>
                                        <td><?php echo $astudent['address']?></td>
                                        <td><img width="60px" src="view/uploads/<?php echo $astudent['image']?>" /></td>


                                        <td class="center">
                                            <a href="view/student/restore.php?id=<?php echo $astudent['uniqe_id']?>">Restore</a> |

                                            <a data-toggle="modal" data-target="#myModal" data-id="<?php echo $astudent['uniqe_id']?>" class="text-danger delete" href="javaScript:void(0)">Delete</a>
                                        </td>
                                    </tr>
                           <?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->

                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="view/student/delete.php" method="get">
                                    <input id="delete" type="hidden" name="id">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ? </h4>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>


















                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
<?php
include_once '../includes/footer.php';
?>