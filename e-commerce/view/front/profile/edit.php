<?php
    include_once('../../../vendor/autoload.php');
    use App\Session;
    use App\Auth;

    Session::init();
    if(Session::checkSession() == false){
        header("Location:".App\Helper::$basePath);
    }
    include_once('../include/header.php');
?>



<?php include_once('../include/footer.php'); ?>
