<?php
if(!class_exists('ComposerAutoloaderInita9f37ab467eb5341600b48f082627546')){
    include_once '../../vendor/autoload.php';

}

$basePath = App\Helper::config('config.basePath');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Electronic Store a Ecommerce Online Shopping Category Bootstrap Responsive Website Template | Home :: w3layouts</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Electronic Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	SmartPhone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- Custom Theme files -->
    <base href="<?php echo $basePath; ?>">

    <link href="assets/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/front/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/front/css/fasthover.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/front/css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="assets/front/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <link rel="stylesheet" href="assets/front/css/jquery.countdown.css" /> <!-- countdown -->
    <!-- //js -->
    <!-- web fonts -->
    <link href='//fonts.googleapis.com/css?family=Glegoo:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //web fonts -->
    <style>
        .cart.box_1 {
            display: block;
            float: right;
            position: relative;
        }
        .w3view-cart {
            background: #ff5063 none repeat scroll 0 0;
            border: medium none;
            border-radius: 7%;
            height: 44px;
            outline: medium none;
            position: relative;
            right: 14%;
            text-align: center;
            width: 50px;
            z-index: 999;
        }
        #cart-box-area {
            background: #fff none repeat scroll 0 0;
            border: 1px solid #ddd;
            clear: left;
            display: none;
            float: none;
            left: -318px;
            padding: 10px;
            position: absolute;
            top: 45px;
            width: 360px;
            z-index: 800;
        }
        #cart-box-area ul > li.sbmincart-item {
            border-bottom: 1px solid #ddd;
            display: block;
            overflow: hidden;
            padding: 19px 5px;
            width: 100%;
        }
        #cart-box-area ul > li.sbmincart-item > div {
            float: left;
        }
    </style>
</head>
<body>

<?php if(App\Session::checkSession() == false){ ?>
<!-- header modal -->
<div class="modal fade" id="myModal88" tabindex="-1" role="dialog" aria-labelledby="myModal88"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;</button>
                <h4 class="modal-title" id="myModalLabel">Don't Wait, Login now!</h4>
            </div>
            <div class="modal-body modal-body-sub">
                <div class="row">
                    <div class="col-md-8 modal_body_left modal_body_left1" style="border-right: 1px dotted #C2C2C2;padding-right:3em;">
                        <div class="sap_tabs">
                            <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                                <ul>
                                    <li class="resp-tab-item" aria-controls="tab_item-0"><span>Sign in</span></li>
                                    <li class="resp-tab-item" aria-controls="tab_item-1"><span>Sign up</span></li>
                                </ul>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                                    <div class="facts">
                                        <div class="register">
                                            <form action="view/front/login.php" method="post">
                                                <input name="email" placeholder="Username Or Email Address" type="text" required="">
                                                <input name="password" placeholder="Password" type="password" required="">
                                                <div class="sign-up">
                                                    <input type="submit" value="Sign in"/>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-2 resp-tab-content" aria-labelledby="tab_item-1">
                                    <div class="facts">
                                        <div class="register">
                                            <form action="view/front/register.php" method="post">
                                                <input placeholder="Name" name="name" type="text" required="">
                                                <input placeholder="Email Address" name="email" type="email" required="">
                                                <input placeholder="Password" name="password" type="password" required="">
                                                <input placeholder="Confirm Password" name="confirmpassword" type="password" required="">
                                                <input placeholder="Your Mobile" name="mobile" type="text" required="">
                                                <textarea placeholder="Your Address" name="address" required=""></textarea>
                                                <div class="sign-up">
                                                    <input type="submit" value="Create Account"/>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="OR" class="hidden-xs">OR</div>
                    </div>
                    <div class="col-md-4 modal_body_right modal_body_right1">
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3 class="other-nw">Sign in with</h3>
                            </div>
                            <div class="col-md-12">
                                <ul class="social">
                                    <li class="social_facebook"><a href="#" class="entypo-facebook"></a></li>
                                    <li class="social_dribbble"><a href="#" class="entypo-dribbble"></a></li>
                                    <li class="social_twitter"><a href="#" class="entypo-twitter"></a></li>
                                    <li class="social_behance"><a href="#" class="entypo-behance"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header modal -->
<?php } ?>

<!-- header -->
<div class="header" id="home1">
    <div class="container">
        <div class="w3l_login">

            <?php if(App\Session::checkSession() == true){ ?>
            <div class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="view/front/profile/index.php">Profile</a></li>
                    <li><a href="view/front/profile/edit.php">Edit Profile</a></li>
                    <li><a href="view/front/order/index.php">Your Orders</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="view/front/logout.php">Sign Out</a></li>
                </ul>
            </div>
            <?php }else{ ?>
                <a href="#" data-toggle="modal" data-target="#myModal88"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
            <?php } ?>
        </div>
        <div class="w3l_logo">
            <h1><a href="index.html">Electronic Store<span>Your stores. Your place.</span></a></h1>
        </div>
        <div class="search">
            <input class="search_box" type="checkbox" id="search_box">
            <label class="icon-search" for="search_box"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></label>
            <div class="search_form">
                <form action="#" method="post">
                    <input type="text" name="Search" placeholder="Search...">
                    <input type="submit" value="Send">
                </form>
            </div>
        </div>
        <div class="cart cart box_1">
            <button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
            <div id="cart-box-area">
                <button class="sbmincart-closer" type="button">×</button>
                <ul>
                    <?php
                        if( (count($cartData)>0) && ($cartData != false) ) {
                            foreach ($cartData as $cartProduct){
                    ?>
                    <li class="sbmincart-item sbmincart-item-changed" data-id="<?php echo $cartProduct['id'] ?>">
                        <div class="sbmincart-details-image">
                            <img src="<?php echo 'view/'.$cartProduct['image'] ?>" alt="" />
                        </div>
                        <div class="sbmincart-details-name">
                            <a class="sbmincart-name" href=""><?php echo $cartProduct['name'] ?></a>
                        </div>
                        <div class="sbmincart-details-quantity">
                            <span>1</span>
                        </div>
                        <div class="sbmincart-details-price">
                            <span class="sbmincart-price"><?php echo '$'.$cartProduct['price'] ?></a></span>
                        </div>
                        <div class="sbmincart-details-remove">
                            <button type="button" class="sbmincart-remove" data-sbmincart-idx="0">×</button>
                        </div>
                    </li>
                    <?php
                            }
                        }
                    ?>
                </ul>
                <div class="sbmincart-footer">
                    <div class="sbmincart-subtotal"><?php echo 'Subtotal: $'.$subtotal.' USD'; ?></div>
                    <a href="view/front/checkout.php" class="checkout">Checkout</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //header -->
<!-- navigation -->
<div class="navigation">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header nav_2">
                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav">
                    <li><a href="index.php" >Home</a></li>
                    <!-- Mega Menu -->
                    <li><a href="view/front/product.php">Product</a></li>
                    <li><a href="view/front/layouts/about.php">About Us</a></li>
                    <li class="w3pages"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="icons.html">Web Icons</a></li>
                            <li><a href="codes.html">Short Codes</a></li>
                        </ul>
                    </li>
                    <li><a href="view/front/mail.php">Mail Us</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- //navigation -->
