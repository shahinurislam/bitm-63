<?php

include_once('../../../vendor/autoload.php');
use App\Session;
use App\Cart\Cart;
Session::init();
$cartObj = new Cart();
if(isset($_POST['cartAction']) && ($_POST['cartAction'] == 'addProduct')
    && (isset($_POST['pid']) && !empty($_POST['pid']) ) ){

    $id = $_POST['pid'];
    $result = $cartObj->addProduct($id);
    if($result === true){
        $cartData = $cartObj->getCartAll();
        $subtotal = $cartObj->getTotalPrice();
        if($cartData !=false){
            $data = array('status'=> true, 'cartData'=>$cartData, 'totalPrice' => $subtotal );
            echo json_encode($data);
        }else{
            $data = array('status'=> true, 'cartData'=>false, 'totalPrice' => $subtotal );
            echo json_encode($data);
        }
    }else{
        $data = array('status'=> false, 'totalPrice' => $subtotal );
        echo json_encode($data);
    }
}

if( isset($_POST['cartAction']) && ($_POST['cartAction'] == 'removeProduct')
    && (isset($_POST['pid']) && !empty($_POST['pid']) ) ){

    $id = $_POST['pid'];
    $result = $cartObj->removeProduct($id);
    $subtotal = $cartObj->getTotalPrice();
    if($result === true){
        $cartData = $cartObj->getCartAll();
        if($cartData !=false){
            $data = array('status'=> true, 'cartData'=>$cartData, 'totalPrice' => $subtotal );
            echo json_encode($data);
        }else{
            $data = array('status'=> true, 'cartData'=>false, 'totalPrice' => $subtotal );
            echo json_encode($data);
        }
    }else{
        $data = array('status'=> false, 'totalPrice' => $subtotal );
        echo json_encode($data);
    }
}




?>