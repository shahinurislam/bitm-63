<?php
include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;


Session::init();

?>
<?php

    if ( Auth::checkRule('Admin') === true || Auth::checkRule('Editor') === true ) {

?>
<?php include_once('include/header.php'); ?>
        
<?php include_once('layouts/dashboard.php'); ?>

<?php include_once('include/footer.php'); ?>

<?php
    }else {
        header('Location: '.App\Helper::config('config.basePath').'view/admin/login.php');
    }
?>