<?php
include_once '../../../vendor/autoload.php';
use App\Auth;
use App\Session;
use App\Media\Media;
Session::init();
$mediaObj = new Media();
$allMedia = $mediaObj->getAllMedia();
$data = array('status'=> true, 'mediaData'=>$allMedia );
echo json_encode($data);
