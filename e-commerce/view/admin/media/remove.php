<?php
include_once '../../../vendor/autoload.php';
use App\Helper;
App\Session::init();

$media = new App\Media\Media();
if($_POST['mediaId']){
    $id = (int) $_POST['mediaId'];
    $result = $media->removeMedia($id);

    if($result == true || $result == 1){
        if($result != false || $result != 0){
            $data = array('status'=> true);
            $data = json_encode($data);
            echo $data;
        }
    }
}

if($_GET['id']){
    $id = (int) $_GET['id'];
    $result = $media->removeMedia($id);

    if($result == true || $result == 1){
        if($result != false || $result != 0){
            header('location:index.php');
        }
    }
}