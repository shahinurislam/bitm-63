<?php
include_once '../../../vendor/autoload.php';
use App\Helper;
App\Session::init();

$media = new App\Media\Media();
$media->setFile($_FILES);
$media->storeFiles();
$result = $media->storeMedia();

if($result != false || $result != 0){
    $data = array('status'=> true, 'mediaId'=>$result );
    $data = json_encode($data);
    echo $data;
}
