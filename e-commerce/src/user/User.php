<?php
/**
 * Created by PhpStorm.
 * User: Vergil
 * Date: 10/3/2017
 * Time: 12:15 AM
 */
namespace App\User;
use App\Database;
use PDO;

class User extends Database {

    protected $table = 'users';

    protected $colums = array('name', 'email', 'password', 'rule', 'activation_key', 'user_status');

    private $sql      = null;

    public function create($data){
//            $keys   = implode(', ', $this->colums);
//            $value  = implode(', :', $this->colums);

            $stmt = $this->con->prepare("INSERT INTO $this->table (name, email, password, rule, activation_key, user_status) VALUES (:name, :email, :password, :rule, :activation_key, :user_status)");

            $stmt->bindValue(":name", $data['name'], PDO::PARAM_STR);
            $stmt->bindValue(":email", $data['email'], PDO::PARAM_STR);
            $stmt->bindValue(":password", md5($data['password']), PDO::PARAM_STR);
            $stmt->bindValue(":rule", 'Customer', PDO::PARAM_STR);
            $stmt->bindValue(":activation_key", rand(3333,1000));
            $stmt->bindValue(":user_status", '1', PDO::PARAM_STR);
            $result = $stmt->execute();
            return $result;
    }

    public function getUserByNameOrEmail($email, $pass){
        $stmt = $this->con->prepare("SELECT * FROM $this->table WHERE (name=:email OR email=:email) AND password=:pass");
        $stmt->bindValue(":email", $email, PDO::PARAM_STR);
        $stmt->bindValue(":pass", $pass, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result;
    }

}