<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 10/2/2017
 * Time: 4:57 PM
 */

namespace App;
use App\Session;
use App\user\User;
use App\Helper;

class Auth{


    public static function login($data){
        if(Session::checkSession() !=true){
            Session::set('auth', true);
            Session::set('user', $data);
        }
    }

    public static function logout(){
        if(Session::checkSession() == true){
            Session::keyUnSet('auth');
            Session::keyUnSet('user');
            Session::destroy();
        }
        header('Location: '.Helper::config('config.basePath'));
    }

    public function checkRule($role){
        if(Session::checkSession() == true){
            $userData = Session::get('user');
            if($userData['rule'] == $role){
                return true;
            }
            return false;
        }
    }

    public static function getUser(){
        if(Session::checkSession() != false ){
            $userdata  = Session::get('user');
            if ($userdata != false){
                return $userdata;
            }
            return false;
        }
        return false;
    }

}