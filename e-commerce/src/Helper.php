<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 10/2/2017
 * Time: 4:58 PM
 */

namespace App;


class Helper{


    public static function config($string){
        return self::getConfigData($string);
    }

    public static function getConfigData($arrayString){//export.store.path
        $array = explode('.', $arrayString);

        if(file_exists(__DIR__.'/../config/'.$array[0].'.php')){
            $data = file_get_contents(__DIR__.'/../config/'.$array[0].'.php');
            $data = eval('?>'.$data);
            for ($i=1; $i<count($array); $i++){
                if((count($array) - 1) == $i){
                    //var_dump((count($array) - 1) ." : ".$i);
                    return $data[$array[$i]];
                }
                $data = $data[$array[$i]];
            }
        }
        //return false;
    }

}