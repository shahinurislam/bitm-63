<?php
$db = new PDO('mysql:host=localhost;dbname=test2;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `cour_form`";

$result = $db->query($query);
$rows = $result->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
		
		<table class="table table-bordered">
		  <thead>
			<tr>
			  <th>SL</th>
			  <th>Course Title</th>
			  <th>Course Code</th>
			  <th>Course Creadit</th>
			  <th>Course Descripton</th>
			  <th>Department</th>
			  <th>Course Duration</th>			
			  <th>Action</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
		  $i=0;
		  foreach($rows as $row){
			  $i++;?>
			<tr>
			  <th scope="row"><?php echo $i?></th>
			  <td><?php echo $row['title'];?></td>
			  <td><?php echo $row['code'];?></td>
			  <td><?php echo $row['creadit'];?></td>
			  <td><?php echo $row['description'];?></td>
			  <td><?php echo $row['department'];?></td>
			  <td><?php 
				$t=time();
				echo " From ".(date("Y-m-d",$t))." <br> ";
			  
			  echo "To".$row['duration'];?></td>
	
			  <td>
			  
			  <a class="text-info" href="view.php?id=<?php echo $row['id']?>">View</a> |
			  
			  <a class="text-primary" href="edit.php?id=<?php echo $row['id']?>">Edit</a> |
			  
			  <a class="text-danger" href="delete.php?id=<?php echo $row['id']?>">Delete</a>
			  
			  
			  </td>
			</tr>
			<?php }?>
		  </tbody>
		</table>
			<h4 class="text-info text-center"><a href="form.php">ADD NEW</a></h4>

	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>