<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
	
	
	<div class="container">
		<div class="row">
			
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h2 class="text-center">Registration Form</h2>
				<hr>
					<form action="single.php" method="post" enctype="multipart/form-data">
					
						 
						  <div class="form-group">
							<label >Email address :</label>
							<input type="text" class="form-control" name="email" required>
							<small id="emailHelp" class="form-text text-muted"></small>
						  </div>
						  
						  <div class="form-group">
							<label >Password :</label>
							<input type="password" class="form-control" name="pass" required>
						  </div>	 

						 
						  
						  <div class="form-group text-right">
								<button type="reset" class="btn btn-info" name="form">Reset</button>
							    <button type="submit" class="btn btn-primary" name="form">Submit</button>
								
						  </div>
				
						  
						
						</form>
			
		
			
			</div>
		</div>
	</div>
	
	

	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>