<?php
$db = new PDO('mysql:host=localhost;dbname=test2;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `user_info`";

$result = $db->query($query);
$rows = $result->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
		
		<table class="table table-bordered">
		  <thead>
			<tr>
			  <th>SL</th>
			  <th>Name</th>
			  <th>ID</th>
			  <th>Email</th>
			  <th>Pass</th>
			  <th>Address</th>
			  <th>Phone</th>
			  <th>Gender</th>
			  <th>Skill</th>
			  <th>hobbie</th>
			  <th>Dob</th>
			  <th>Image</th>
			  <th>Action</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
		  $i=0;
		  foreach($rows as $row){
			  $i++;?>
			<tr>
			  <th scope="row"><?php echo $i?></th>
			  <td><?php echo $row['name'];?></td>
			  <td><?php echo $row['student_id'];?></td>
			  <td><?php echo $row['email'];?></td>
			  <td><?php echo $row['pass'];?></td>
			  <td><?php echo $row['address'];?></td>
			  <td><?php echo $row['number'];?></td>
			  <td><?php echo $row['gender'];?></td>
			  <td><?php echo $row['skill'];?></td>
			  <td><?php echo $row['hobbies'];?></td>
			  <td><?php echo $row['dob'];?></td>
			  <td><img width="50px" src="uploads/<?php echo $row['image'];?>"/></td>
						
			  <td>
			  
			  <a class="text-info" href="view.php?id=<?php echo $row['id']?>">View</a> |
			  
			  <a class="text-primary" href="edit.php?id=<?php echo $row['id']?>">Edit</a> |
			  
			  <a class="text-danger" href="delete.php?id=<?php echo $row['id']?>">Delete</a>
			  
			  
			  </td>
			</tr>
			<?php }?>
		  </tbody>
		</table>
			<h4 class="text-info text-center"><a href="form.php">ADD NEW</a></h4>

	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>