<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
	
	
	<div class="container">
		<div class="row">
			
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h2 class="text-center">Registration Form</h2>
				<hr>
					<form action="store.php" method="post" enctype="multipart/form-data">
					
						  <div class="form-group">
							<label >Name :</label>
							<input type="text" class="form-control" name="name" required>
							<input type="hidden" class="form-control" name="student_id" value="S">
							<small id="emailHelp" class="form-text text-muted"></small>
						  </div>
						  
						  <div class="form-group">
							<label >Email address :</label>
							<input type="text" class="form-control" name="email" required>
							<small id="emailHelp" class="form-text text-muted"></small>
						  </div>
						  
						  <div class="form-group">
							<label >Password :</label>
							<input type="password" class="form-control" name="pass" required>
						  </div>	 

						  <div class="form-group">
							<label >Enter Your Address :</label>
							<input type="textarea" class="form-control" name="address" required>
						  </div>
						  
						  <div class="form-group">
							<label >Enter Your Phone Number :</label>
							<input type="text" class="form-control" name="number" required>
						  </div>
						  
						  	  
						  <div class="form-group">
						  <label >Select Your Gender :</label>
						

								<label class="custom-control custom-radio">
								  <input id="radio1" name="gender" type="radio" class="custom-control-input"  value="Male" required>
								  <span class="custom-control-indicator"></span>
								  <span class="custom-control-description">Male</span>
								</label>
								<label class="custom-control custom-radio">
								  <input id="radio2" name="gender" type="radio" class="custom-control-input"  value="Female" required>
								  <span class="custom-control-indicator"></span>
								  <span class="custom-control-description">Female</span>
								</label>
								
						  </div> 
						  
						  
						  <div class="form-group">
							<label >Skill :</label>
						
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="skill[]" value="php" >
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">php</span>
								</label>
							
					
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="skill[]" value="css" >
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">css</span>
								</label>
							
						
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="skill[]" value="html" >
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">html</span>
								</label>
							
						  </div>
						
						  

						  <div class="form-group">
							<label >Choose your hobbies :</label>
						
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="hobbie[]" value="cricket" >
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">cricket</span>
								</label>
						
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="hobbie[]" value="football" >
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">football</span>
								</label>
							
					
								<label class="custom-control custom-checkbox">
									  <input type="checkbox" class="custom-control-input" name="hobbie[]" value="kabadi" >
									  <span class="custom-control-indicator"></span>
									  <span class="custom-control-description">kabadi</span>
								</label>
							
						  </div>
						  
						<div class="form-group">
							<label >Date of Birth :</label>
							<div class="row">
								<div class="col-md-4">
									<select class="form-control" name="birth[]" value="day" >
										 <?php for($i=1;$i<=31;$i++){
											
											  echo "<option>$i</option>";
										  }
										  
										  ?>
									</select>	
								</div>
								
							
								
							<div class="col-md-4">
								<select class="form-control" name="birth[]" value="month" >
								 <?php for($i=1;$i<=12;$i++){
											
											  echo "<option>$i</option>";
										  }
										  
										  ?>
						
								</select>	
							</div>
							
							
							
							<div class="col-md-4">
							<select class="form-control" name="birth[]" value="years" >
							  <?php for($i=1901;$i<=2017;$i++){ ?>
											
											  <option> <?php echo $i; ?> </option>
										<?php  }
										  
										  ?>
							
							</select>	
							</div>
								
						
							
							
							
						  </div>
						</div>
						  
						  
						  <div class="form-group">
								<label class="custom-file">
								  <input type="file" id="file2" class="custom-file-input" name="image" >
								  <span class="custom-file-control"></span>
								</label>
							
						  </div>
						  
						  <div class="form-group text-right">
								<button type="reset" class="btn btn-info" name="form">Reset</button>
							    <button type="submit" class="btn btn-primary" name="form">Submit</button>
								
						  </div>
				
						  
						
						</form>
			
		
			
			</div>
		</div>
	</div>
	
	

	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>